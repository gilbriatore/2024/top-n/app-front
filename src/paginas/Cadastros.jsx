import axios from "axios";
import { useState, useEffect } from "react";

function Cadastros() {
  const [cliente, setCliente] = useState(null);
  const [clientes, setClientes] = useState([]);

  function getClientes() {
    axios.get("http://localhost:5229/clientes").then((resposta) => {
      //console.log(resposta.data);
      setClientes(resposta.data);
    });
  }

  useEffect(getClientes, []);

  function novoCliente() {
    setCliente({
      nome: "",
      cpf: "",
      telefone: "",
      email: "",
    });
  }

  function cancelar() {
    setCliente(null);
  }

  function refresh() {
    cancelar();
    getClientes();
  }

  function onChangeCliente(campo, valor, id) {
    cliente[campo] = valor;
    setCliente({
      ...cliente,
    });
  }

  function salvarCliente() {
    console.log(cliente);
    if (cliente.id) {
      axios
        .put("http://localhost:5229/clientes/" + cliente.id, cliente)
        .then(() => {
          //refresh();
        });
    } else {
      axios.post("http://localhost:5229/clientes", cliente).then(() => {
        //refresh();
      });
    }
  }

  function getFormulario() {
    return (
      <form>
        <label htmlFor="nome">Nome</label>
        <input
          type="text"
          id="nome"
          name="nome"
          value={cliente.nome}
          onChange={(e) => {
            onChangeCliente(e.target.name, e.target.value, cliente.id);
          }}
        />
        <br />

        <label htmlFor="cpf">CPF</label>
        <input
          type="text"
          id="cpf"
          name="cpf"
          value={cliente.cpf}
          onChange={(e) => {
            onChangeCliente(e.target.name, e.target.value, cliente.id);
          }}
        />
        <br />

        <label htmlFor="cpf">Telefone</label>
        <input
          type="text"
          id="telefone"
          name="telefone"
          value={cliente.telefone}
          onChange={(e) => {
            onChangeCliente(e.target.name, e.target.value, cliente.id);
          }}
        />
        <br />

        <label htmlFor="cpf">E-mail</label>
        <input
          type="text"
          id="email"
          name="email"
          value={cliente.email}
          onChange={(e) => {
            onChangeCliente(e.target.name, e.target.value, cliente.id);
          }}
        />
        <br />

        <button
          onClick={() => {
            salvarCliente();
          }}
        >
          Salvar
        </button>
        <button
          onClick={() => {
            cancelar();
          }}
        >
          Cancelar
        </button>
      </form>
    );
  }

  function excluirCliente(id) {
    axios.delete("http://localhost:5229/clientes/" + id).then(() => {
      getClientes();
    });
  }

  function editarCliente(cliente) {
    setCliente(cliente);
  }

  function getLinhaDaTabela(index, cliente) {
    return (
      <tr key={index}>
        <td>{cliente.id}</td>
        <td>{cliente.nome}</td>
        <td>{cliente.cpf}</td>
        <td>{cliente.telefone}</td>
        <td>{cliente.email}</td>
        <td>
          <button
            onClick={() => {
              excluirCliente(cliente.id);
            }}
          >
            Excluir
          </button>
          <button
            onClick={() => {
              editarCliente(cliente);
            }}
          >
            Editar
          </button>
        </td>
      </tr>
    );
  }

  function getLinhasDaTabela() {
    // const pessoas = [
    //       {id:"1",nome:"Maria",cpf:"12345"},
    //       {id:"2",nome:"Pedro",cpf:"34567"},
    //       {id:"1",nome:"Maria",cpf:"12345"},
    //       {id:"2",nome:"Pedro",cpf:"34567"},
    //       {id:"1",nome:"Maria",cpf:"12345"},
    //       {id:"2",nome:"Pedro",cpf:"34567"}

    //     ];

    const linhasDaTabela = [];

    for (let i = 0; i < clientes.length; i++) {
      const cliente = clientes[i];
      linhasDaTabela[i] = getLinhaDaTabela(i, cliente);
    }

    return linhasDaTabela;
  }

  function getTabela() {
    return (
      <table>
        <thead>
          <tr>
            <th>ID</th>
            <th>Nome</th>
            <th>CPF</th>
            <th>Telefone</th>
            <th>E-mail</th>
            <th>Ações</th>
          </tr>
        </thead>
        <tbody>{getLinhasDaTabela()}</tbody>
      </table>
    );
  }

  function getConteudo() {
    if (cliente == null) {
      return (
        <>
          <button
            type="button"
            onClick={() => {
              novoCliente();
            }}
          >
            Novo cliente
          </button>
          {getTabela()}
        </>
      );
    } else {
      return getFormulario();
    }
  }

  return (
    <div className="principal">
      <h3>Cadastro de clientes</h3>
      {getConteudo()}
    </div>
  );
}

export default Cadastros;
